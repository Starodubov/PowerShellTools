function Convert-HashTable-String{
      param(
            [hashtable] $Table,
            [string] $PairDelimiter = " ",
            [string] $BlockDelimiter = " "
      )
      
      process{
            if($Table -eq $null -or $Table.Count -eq 0){
                  return "";
            } else {
                  $params = $Table.GetEnumerator()  | %{ "$($_.Name)$PairDelimiter$(if($_.Value -ne $null){$_.Value})" };
                  return [system.String]::Join($BlockDelimiter, $params);
            }
      }
}

function Url-MakeShareFolder{
	param(  
		[Parameter(Position=0, Mandatory=$true, ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)]
		[String]$serverName
	) 
	process {
		return Join-Path '\\' $serverName;
	}
}
