function Execute-SQL-Batch {
	param( 
	        #Name of MS SQL Server instance 
	        [parameter(Mandatory=$true, 
	               HelpMessage="Specify the SQL Server name where will be run a T-SQL code",Position=0)] 
	        [String] 
	        [ValidateNotNullOrEmpty()] 
	        $server = $(throw "sqlserver parameter is required."), 
	 
	        #Database name for execution context 
	        [parameter(Mandatory=$true, 
	               HelpMessage="Specify the context database name",Position=1)] 
	        [String] 
	        [ValidateNotNullOrEmpty()] 
	        $dbname = $(throw "dbname parameter is required."), 
	 
	        #Name of T-SQL file (.sql) 
	        [parameter(Mandatory=$true, 
	               HelpMessage="Specify the name of T-SQL file (*.sql) which will be run",Position=2)] 
	        [String] 
	        [ValidateNotNullOrEmpty()] 
	        $file = $(throw "sqlfile parameter is required."), 
	 
	        #The GO switch. Must be specified if T-SQL code is contain the GO instructions 
	        [parameter(Mandatory=$false,Position=3)] 
	        [Switch] 
	        [AllowEmptyString()] 
	        $go, 
			
	        #info log. Can be specified if full execution log needed
	        [parameter(Mandatory=$false,Position=4)] 
	        [Switch]
	        [AllowEmptyString()] 
	        $info, 			
	 
	        #MS SQL Server user name 
	        [parameter(Mandatory=$false,Position=5)] 
	        [String] 
	        [AllowEmptyString()]
	        $u, 
	 
	        #MS SQL Server password name 
	        [parameter(Mandatory=$false,Position=6)] 
	        [String] 
	        [AllowEmptyString()] 
	        $p,

            #Timeout in seconds, 0 = no timeout
            [parameter(Mandatory=$false, Position=7)]
            [Int]
            $timeout = 0
	    ) 
	#Connect to MS SQL Server 
	try 
	{ 
	    $SQLConnection = New-Object System.Data.SqlClient.SqlConnection 
	    #The MS SQL Server user and password is specified 
	    if($u -and $p) 
	    { 
			Write-Host "Using the user and password is specified"
	        $SQLConnection.ConnectionString = "Server=" + $server + ";Database="  + $dbname + ";User ID= "  + $u + ";Password="  + $p + ";" 
	    } 
	    #The MS SQL Server user and password is not specified - using the Windows user credentials 
	    else 
	    { 
			Write-Host "Using the Windows user credentials"
	        $SQLConnection.ConnectionString = "Server=" + $server + ";Database="  + $dbname + ";Integrated Security=True" 
	    } 
	    $SQLConnection.Open() 
	} 
	#Error of connection 
	catch 
	{ 
	    Write-Host $Error[0] -ForegroundColor Red 
	    exit 1 
	} 
	#The GO switch is specified - parsing T-SQL code with GO 
	if($go) 
	{ 
		$IsSQLErr = $false;	
		$goregex = [regex] '^\s*[Gg][Oo]\s*$';
	    $SQLCommandText = @(Get-Content -Path $file);
		$skippedSqlPackages = 0;
		
		for($i=0; $i -le $SQLCommandText.Length + 1; $i++)
	    { 
			$SQLString = $SQLCommandText[$i];
	        if($goregex.Match($SQLString).Success -eq $false -and $i -le $SQLCommandText.Length) 
	        { 
	            #Preparation of SQL packet 
	            $SQLPacket += $SQLString + "`n" 
	        } 
	        else {
				if($SQLPacket -ne "`n" -and $SQLPacket -ne "") {
					if ($info)
					{
			            Write-Host "---------------------------------------------" 
			            Write-Host "Executed SQL packet:" 
			            Write-Host $SQLPacket
					}
		            #Execution of SQL packet 
		            try 
		            { 
		                $SQLCommand = New-Object System.Data.SqlClient.SqlCommand($SQLPacket, $SQLConnection)
                        $SQLCommand.CommandTimeout = $timeout
		                $SQLCommand.ExecuteScalar()
		            } 
		            catch 
		            { 
		 
		                $IsSQLErr = $true 
						Write-Host "---------------------------------------------" 
						Write-Host "←[31mError:" 
		                Write-Host $Error[0] -ForegroundColor Red 
			            Write-Host "←[31mSQL packet:" 
			            Write-Host $SQLPacket
						Write-Host "---------------------------------------------"
		            } 
		            $SQLPacket = "" 
				} else {
					$skippedSqlPackages++;					
				}
	        } 
	    }
	    if(-not $IsSQLErr) 
        { 	
			if($info){
				Write-Host "---------------------------------------------" 
            	Write-Host "←[34mExecution succesful" 
			}
        } 
        else 
        { 
			Start-Sleep 300;
			Write-Host "---------------------------------------------" 
            Write-Error "Execution failed due to SQL errors. All errors was logged."
        } 
	} 
	else 
	{ 
	    #Reading the T-SQL file as a whole packet 
	    $SQLCommandText = @([IO.File]::ReadAllText($file)) 
	    #Execution of SQL packet 
	    try 
	    { 
	        $SQLCommand = New-Object System.Data.SqlClient.SqlCommand($SQLCommandText, $SQLConnection)
            $SQLCommand.CommandTimeout = $timeout
	        $SQLCommand.ExecuteScalar() 
	    } 
	    catch 
	    { 
	        Write-Error $Error[0] -ForegroundColor Red 
	    } 
	} 
	#Disconnection from MS SQL Server 
	$SQLConnection.Close() 
	Write-Host "-----------------------------------------" 
	Write-Host "Skipped sql pacakges: $skippedSqlPackages"
	Write-Host $file "execution done"
}