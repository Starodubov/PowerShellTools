$__RootPath = Split-Path -parent $PSCommandPath
$__RootPath = Join-Path $__RootPath ".."

."$__RootPath\Tools\CommonTools.ps1";

function Get-SQL-Files{
	param (
		[Parameter(Position=0, Mandatory=$true, ValueFromPipeline=$true, ValueFromPipelineByPropertyName=$true)]
        [string] $Source,
		[Parameter(Position=1)]
		[switch] $Recurse
	)
	process{
		Get-ChildItem -path $Source -File |
			?{[string]$_ -like "*.sql"} |
			Sort-Object |
			Write-Output
		
		if ($Recurse){
			Get-ChildItem -path $Source -Directory | 
				Join-Path $Source -ChildPath {$_} |
				Get-SQL-Files -Recurse:$Recurse
		}
	}
}

function Compile-SQL-File {
	param(
        [string] $Source,
		[string] $Target,
		[switch] $Recurse
	)
	process{
        if (Test-Path $Target){
        	Remove-Item $Target -Verbose;
        }

		Get-SQL-Files -Source $Source -Recurse:$Recurse |
			%{ Out-File -filepath $Target -Encoding "UTF8" -inputobject ([System.IO.File]::ReadAllText($_.fullname) + "`r`nGO`r`n") -Append}
			
        if (Test-Path $Target){
            Write-Host "Created file [$Target]"
        }	
	}
}
